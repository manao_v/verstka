$(document).ready(function() {
    $("#fullpage").addClass("active").fullpage({
        anchors: ['all', 'tochnee', 'profy', 'newbee', 'webinars', 'feedback', '3steps'],
        navigation: true,
    });
    $("#fp-nav").css({"margin-top": +$("#fp-nav").css("margin-top").substring($("#fp-nav").css("margin-top").length - 2, -2) + 30 + "px"});

    if ($(window).width() < 800) {
        $.fn.fullpage.destroy('all');
    };

    $(window).resize(function() {
        if ($(window).width() < 800 && $("#fullpage").hasClass("active")) {
            $.fn.fullpage.destroy('all');
            $("#fullpage").removeClass("active");
        } else if ($(window).width() >= 800 && !$("#fullpage").hasClass("active")) {
            $("#fullpage").addClass("active").fullpage({
                anchors: ['all', 'tochnee', 'profy', 'newbee', 'webinars', 'feedback', '3steps'],
                navigation: true,
            });
        }
    });

});